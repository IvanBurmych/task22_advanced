let submit = document.getElementById("submit");

submit.addEventListener("click", () => {
    let table = document.getElementById("table");
    if(table !== null)
        document.body.removeChild(table);
});

submit.addEventListener("click", () => {
    let form = document.myForm;
    let rowsCount = parseInt(form.rows.value);
    let columnsCount = parseInt(form.columns.value);
    var myColors = ['red', 'purple', '#E84751', 'blue', 'orange', '#323643'];

    if(isNaN(rowsCount) || isNaN(columnsCount) || rowsCount < 1 || columnsCount < 1) {
        alert("Incorrect");
    } else {
        let table = document.createElement("table");
        table.id = "table";
    
        for(let i = 1; i < rowsCount + 1; i++){
            let elem = document.createElement("tr");
            for(let j = 1; j < columnsCount + 1; j++){
                let cell = document.createElement("td");
                elem.appendChild(cell);
            }
            table.appendChild(elem);
        }
        table.onclick = function(event) {
            let td = event.target.closest('td');
          
            if (!td) return; // (2)
          
            if (!table.contains(td)) return;
          
            td.style.background = myColors[Math.floor(Math.random()*myColors.length)];
        };
        
        document.body.appendChild(table);
    }
});

submit.addEventListener("click", () => {
    let form = document.myForm;
    let rowsCount = parseInt(form.rows.value);
    let columnsCount = parseInt(form.columns.value);

    if (!isNaN(rowsCount) && !isNaN(columnsCount)) {
        let table = document.getElementById("table");
        let rowArray = table.getElementsByTagName("tr");

        for(let i = 1; i < rowArray.length + 1; i++){
            let columnArray = rowArray[i-1].getElementsByTagName("td");

            for(let j = 1; j < columnArray.length + 1; j++){
                let text = document.createTextNode(i.toString()+j.toString());
                columnArray[j-1].appendChild(text);
            }
        }
    }
});
